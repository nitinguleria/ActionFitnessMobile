package com.actionfitness.menu;

import com.actionfitness.R;
import com.actionfitness.R.drawable;
import com.actionfitness.R.id;
import com.actionfitness.R.layout;
import com.actionfitness.rings.PlayActivity;
import com.actionfitness.wheel.WheelActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

public class GameMenuActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
	            WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_game_menu);
	}
	
	public void onExercise(View v) 
	{
        int id = v.getId();
        switch (id) {
        case R.id.buttonpullups:{
        	//v.setBackgroundResource(R.drawable.pulluponringsmouseover);

        	Intent intent = new Intent(this, PlayActivity.class);
        	startActivity(intent);
        	break;
        }
        case R.id.buttonpushups:{
        	//v.setBackgroundResource(R.drawable.dopushupsmouseover);

        	Intent intent = new Intent(this, WheelActivity.class);

        	startActivity(intent);
        	break;
        }
        }
	}
}
