package com.actionfitness.wheel;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

import android.util.Log;

public class Toilet {
	
	//circle variables
	int circlePoints=40;
	float radius=2.5f;
	float outerRadius=3.0f;
	float center_x=0.0f;
	float center_y=0.0f;
	//outer vertices of the circle i.e. excluding the center_x,center_y
    int circumferencePoints=circlePoints-1;
    
	//circle vertices and buffer variables 
	int vertices=0;
	float circleVertices[]= new float[circlePoints*2]; 
	private FloatBuffer toiletBuff; //4bytes per float

	//outer circle vertices and buffer variables
	int outerVertices=0;
	float outerCircleVertices[]= new float[circlePoints*2]; 
	private FloatBuffer outerToiletBuff; //4bytes per float
	
	//color values	
	 private float rgbaValues[]={
	    		0,0,0,0
	    				
	    };
	    private FloatBuffer colorBuff;
	
	// debug variables
	private static final String TAG = "Toilet";
	private boolean debug = false;
	
     public Toilet()
     {
    	 //the initial  values
    	 circleVertices[vertices++] = center_x;
    	   circleVertices[vertices++] = center_y;

    	  //outer  circle variables
    	   outerCircleVertices[outerVertices++] = center_x;
    	   outerCircleVertices[outerVertices++] = center_y;
    	   
    	 //set circle vertices values 
   		for (int i = 0; i < circumferencePoints; i++)
   		{
       	
   			float percent = (i / (float) (circumferencePoints - 1));
   			float radians = (float) (percent * 2 * Math.PI);

   			// vertex position
   			float outer_x = (float) (center_x + radius * Math.cos(radians));
   			float outer_y = (float) (center_y + radius * Math.sin(radians));

   			float outerCircle_x = (float) (center_x + outerRadius * Math.cos(radians));
   			float outerCircle_y = (float) (center_y + outerRadius * Math.sin(radians));
   			
   			circleVertices[vertices++] = outer_x;
   			circleVertices[vertices++] = outer_y;
   			
   			outerCircleVertices[outerVertices++] = outerCircle_x;
   			outerCircleVertices[outerVertices++] = outerCircle_y;
   			
   			if(debug)
   			{
   				Log.d(TAG,"x :" + i+" "+ outer_x + " y : "+i+" "+ outer_y );
   			}
   			
   			
   		} 
   		
   		
   		if(debug)
   		{
   			for(int i=0;i<circleVertices.length-1;i=i+2)
   			{
   				Log.d(TAG,"circleVertices array values x :" + i + " " + circleVertices[i] + " y : "+ (i+1)+ " "+ circleVertices[i+1]);
   			}
   		}
		
    	
		
		// float buffer short has 4 bytes
		ByteBuffer toiletByteBuff = ByteBuffer
				.allocateDirect(circleVertices.length * 4);
		// garbage collector wont throw this away
		toiletByteBuff.order(ByteOrder.nativeOrder());
		toiletBuff = toiletByteBuff.asFloatBuffer();
		toiletBuff.put(circleVertices);
		toiletBuff.position(0);
		
		
		// float buffer short has 4 bytes
				ByteBuffer toiletOuterByteBuff = ByteBuffer
						.allocateDirect(outerCircleVertices.length * 4);
				// garbage collector wont throw this away
				toiletOuterByteBuff.order(ByteOrder.nativeOrder());
				outerToiletBuff = toiletOuterByteBuff.asFloatBuffer();
				outerToiletBuff.put(outerCircleVertices);
				outerToiletBuff.position(0);

		// float buffer short has 4 bytes
				ByteBuffer clrByteBuff = ByteBuffer.allocateDirect(rgbaValues.length * 4);
				// garbage collector wont throw this away
				clrByteBuff.order(ByteOrder.nativeOrder());
				colorBuff = clrByteBuff.asFloatBuffer();
				colorBuff.put(rgbaValues);
				colorBuff.position(0);
				
    
   }
     
   //draw methods
 	public void draw(GL10 gl){
 		
 		      //get the front face
 				gl.glFrontFace(GL10.GL_CW); //circle formed in clockwise  direction
 				gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
 				
 				//enable color array
 				//gl.glEnableClientState(GL10.GL_COLOR_ARRAY);
 				
 				//pointer to the buffer
 				//gl.glVertexPointer(2, GL10.GL_FLOAT, 0, outerToiletBuff);
 				gl.glVertexPointer(2, GL10.GL_FLOAT, 0, toiletBuff);
 				

 				//pointer to color
 				//gl.glColorPointer(4, GL10.GL_FLOAT, 0, colorBuff);
 				
 				//draw hollow circle
 				//gl.glDrawArrays(GL10.GL_LINE_LOOP, 1, circlePoints);

 				//draw circle as filled shape
 				gl.glDrawArrays(GL10.GL_TRIANGLE_FAN, 0, circlePoints);
 				

 				
 				//gl.glDisableClientState(GL10.GL_COLOR_ARRAY);
 				gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
 				
 	}
 	
 	 //draw methods
 	public void drawSeat(GL10 gl){
 		
 		      //get the front face
 				gl.glFrontFace(GL10.GL_CW); //circle formed in clockwise  direction
 				gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
 				
 				//enable color array
 				gl.glEnableClientState(GL10.GL_COLOR_ARRAY);
 				
 				//pointer to the buffer
 				gl.glVertexPointer(2, GL10.GL_FLOAT, 0, outerToiletBuff);
 				//gl.glVertexPointer(2, GL10.GL_FLOAT, 0, toiletBuff);
 				

 				//pointer to color
 				gl.glColorPointer(4, GL10.GL_FLOAT, 0, colorBuff);
 				
 				//draw hollow circle
 				//gl.glDrawArrays(GL10.GL_LINE_LOOP, 1, circlePoints);

 				//draw circle as filled shape
 				gl.glDrawArrays(GL10.GL_TRIANGLE_FAN, 0, circlePoints);
 				

 				
 				gl.glDisableClientState(GL10.GL_COLOR_ARRAY);
 				gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
 				
 	}
     
     
}
