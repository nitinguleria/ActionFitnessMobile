package com.actionfitness.rings;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar.LayoutParams;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.actionfitness.R;
import com.actionfitness.graphing.RealtimeChartSurfaceView;
import com.actionfitness.menu.GameMenuActivity;

public class ResultsActivity extends Activity {
	
	// UI elements
	static LinearLayout angleGraphLayout = null;
	static TextView finalScoreBox = null;
	static Button startStopButton = null;

	// Chart objects
	private RealtimeChartSurfaceView angleChart = null;

	// Data and lists
	public List<Double> angleList;
	private float[] angleData;
	double bucketFill = 0;
	long runningTime = 0;
	double finalScore = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
	            WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_results);
		
		// Get layout objects
		angleGraphLayout = (LinearLayout) findViewById(R.id.angleResultsLayout);
		finalScoreBox = (TextView) findViewById(R.id.finalInfoBox);
		
		// Add angle chart to the screen
		angleChart = new RealtimeChartSurfaceView(this);
		LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		angleGraphLayout.addView(angleChart, params1);
		
		// Get chart data from previous activity
		bucketFill = PlayActivity.currentPercent;
		runningTime = PlayActivity.runningTime;
		angleList = PlayActivity.angleList;
		startStopButton = (Button) findViewById(R.id.start_stop_button);

		// Check that at least some data exists
		if(angleList.size() <= 0) {
			finalScoreBox.setText("Error: no data");
			return;
		}
		
		// Convert data from lists to arrays, compressing everything to 350 values
		angleData = new float[600];
		for(int i = 0; i < 600; i ++) {
			angleData[i] = angleList.get((int) Math.round(i * ((double)(angleList.size() - 1) / 600))).floatValue();
			finalScore += angleData[i];
		}
		
		// Draw graphs with data
		angleChart.setChartData(angleData);
		
		// Set final score
		finalScoreBox.setText("Bucket: " + ((double)Math.round(bucketFill * 10) / 10) + "%   "
				+ "Time: " + ((double)Math.round(runningTime / 100) / 10) + " s   "
				+ "Score: " + Math.round(finalScore));
	}
	
	public void startStopPressed(View v) {
		Intent intent= new Intent(this,GameMenuActivity.class);
		startActivity(intent);

	}
	@Override
	public void onBackPressed() {
		//super.onBackPressed();
		finish();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.results, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_help) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
