ActionFitnessMobile
===================

An app based on time integral of distance( called absement) for  android mobile  for  actional fitness. 
Suitable for gym equipments like rings for pull ups as well as doing  push ups on a flat wheel. 

See the paper on http://wearcam.org/mannfit. 

The application uses openGL and android magnetometer data for orientation purposes. For any other info contact the developer.
